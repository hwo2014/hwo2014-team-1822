package noobbot.messages;

import java.util.List;

public class CarPositions {
    public String msgType;
    public List<CarPosition> data;
    public String gameId;
    public int gameTick;

    private static String myCarColor = null;

    public static void setMyColor(String color) {
        myCarColor = color;
    }

    public CarPosition getMyCar() {
        if(myCarColor == null) {
            return null;
        }
        for (CarPosition car : data) {
            if(car.id.color.equals(myCarColor)) {
                return car;
            }
        }
        return null;
    }
}
